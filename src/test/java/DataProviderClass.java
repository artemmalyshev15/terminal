import Enums.Currency;
import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider
    public static Object[][] testNegativeDataForFrodMonitorTest() {
        return new Object[][]{
                {2050},
                {3000}
        };
    }

    @DataProvider
    public static Object[][] testPositiveDataForFrodMonitorTest() {
        return new Object[][]{
                {500}
        };
    }

    @DataProvider
    public static Object[][] testPositiveDataForTerminalImplTest() {
        return new Object[][]{
                {"12345678912345678912", 123},
                {"23456789123456789123", 111}
        };
    }

    @DataProvider
    public static Object[][] testNegativeDataForTerminalImplTest() {
        return new Object[][]{
                {"12345678912345678912", 11},
                {"23456789123456789123", 1}
        };
    }

    @DataProvider
    public static Object[][] testPositiveDataTransferForTerminalImplTest() {
        return new Object[][]{
                {500, Currency.RUR, "23456789123456789123"}
        };
    }
}
