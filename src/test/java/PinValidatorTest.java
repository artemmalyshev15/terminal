import Persons.Person;
import Terminal.Exceptions.AccountIsLockedException;
import Terminal.PinValidator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PinValidatorTest {

    private Person sender;
    private PinValidator pinValidator;

    @BeforeMethod
    public void setUp() {
        sender = new Person("Иванов", "Иван", "Иванович",
                "12345678912345678912", "1234567890123456", 5000, 123);
        pinValidator = new PinValidator();
    }

    @Test(expectedExceptions = AccountIsLockedException.class)
    public void validateNegativeTest() throws AccountIsLockedException, IllegalAccessException {
        for (int i = 0; i < 3; i++) {
            try {
                pinValidator.validate(sender, 12);
            } catch (IllegalAccessException e) {
                Assert.assertEquals("Ошибка доступа. Пароль не верный. Попробуйте еще раз.", e.getMessage());
            } catch (AccountIsLockedException e) {
                Assert.assertEquals("Ваш аккаунт заблокирован на 5 секунд.", e.getMessage());
            }
        }
        pinValidator.validate(sender, 12);
    }

    @Test
    public void validatePositiveTest() throws AccountIsLockedException, IllegalAccessException {
        pinValidator.validate(sender, 123);
        Assert.assertEquals(sender.getPinCode(), 123);
    }
}
