import Enums.Currency;
import Terminal.Exceptions.AccountIsLockedException;
import Terminal.Exceptions.IllegalSumException;
import Terminal.PinValidator;
import Terminal.TerminalImpl;
import Terminal.TerminalServer;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TerminalImplTest {

    @Mock
    private TerminalImpl terminal;

    @BeforeClass
    public void setUp() {
        terminal = new TerminalImpl();
    }

    @Test(dataProvider = "testPositiveDataForTerminalImplTest", dataProviderClass = DataProviderClass.class)
    public void loginPositiveTest(String numberAcc, int pinCode) throws AccountIsLockedException, IllegalAccessException {
        terminal.login(numberAcc, pinCode);
    }

    @Test(expectedExceptions = {IllegalAccessException.class},
            dataProvider = "testNegativeDataForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void loginNegativeTest(String numberAcc, int pinCode) throws AccountIsLockedException, IllegalAccessException {
        terminal.login(numberAcc, pinCode);
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransferAccToAccValidTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws AccountIsLockedException, IllegalAccessException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        try {
            terminal.doTransferAccToAcc(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            if ("Ошибка сети. Нет подключения к сети.".equals(e.getMessage())) {
                System.out.println("Ошибка подключения отловлена.");
            }
        }
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransAccToAccIfNotConnectionTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws IllegalAccessException, AccountIsLockedException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        TerminalServer terminalServer = Mockito.mock(TerminalServer.class);
        Mockito.doThrow(new IllegalAccessException()).when(terminalServer).isConnection();
        try {
            terminal.doTransferAccToAcc(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            Assert.assertEquals("Ошибка сети. Нет подключения к сети.", e.getMessage());
        }
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransAccToAccIfPersonNullTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws IllegalAccessException, AccountIsLockedException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        PinValidator pinValidator = Mockito.mock(PinValidator.class);
        pinValidator.validate(null, 123);
        try {
            terminal.doTransferAccToAcc(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            if ("Ошибка сети. Нет подключения к сети.".equals(e.getMessage())) {
                System.out.println("Ошибка подключения отловлена.");
            } else
                Assert.assertEquals("Вы не авторизованы. Авторизуйтесь для работы в системе.", e.getMessage());
        }
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransferCardToCardValidTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws AccountIsLockedException, IllegalAccessException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        try {
            terminal.doTransferCardToCard(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            if ("Ошибка сети. Нет подключения к сети.".equals(e.getMessage())) {
                System.out.println("Ошибка подключения отловлена.");
            }
        }
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransCardToCardIfNotConnectionTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws IllegalAccessException, AccountIsLockedException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        TerminalServer terminalServer = Mockito.mock(TerminalServer.class);
        Mockito.doThrow(new IllegalAccessException()).when(terminalServer).isConnection();
        try {
            terminal.doTransferCardToCard(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            Assert.assertEquals("Ошибка сети. Нет подключения к сети.", e.getMessage());
        }
    }

    @Test(dataProvider = "testPositiveDataTransferForTerminalImplTest",
            dataProviderClass = DataProviderClass.class)
    public void doTransCardToCardIfPersonNullTest(double transactionSum, Currency currency, String recipientAccountNumber)
            throws IllegalAccessException, AccountIsLockedException, IllegalSumException {
        terminal.login("12345678912345678912", 123);
        PinValidator pinValidator = Mockito.mock(PinValidator.class);
        pinValidator.validate(null, 123);
        try {
            terminal.doTransferCardToCard(transactionSum, currency, recipientAccountNumber);
        } catch (IllegalAccessException e) {
            if ("Ошибка сети. Нет подключения к сети.".equals(e.getMessage())) {
                System.out.println("Ошибка подключения отловлена.");
            } else
                Assert.assertEquals("Вы не авторизованы. Авторизуйтесь для работы в системе.", e.getMessage());
        }
    }
}
