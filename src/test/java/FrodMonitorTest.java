import Terminal.Exceptions.IllegalSumException;
import Terminal.FrodMonitor;
import org.testng.annotations.Test;

public class FrodMonitorTest {

    private FrodMonitor frodMonitor = new FrodMonitor();

    @Test(expectedExceptions = IllegalSumException.class, dataProvider = "testNegativeDataForFrodMonitorTest", dataProviderClass = DataProviderClass.class)
    public void isValidTransactionNegativeTest(double transactionSum) throws IllegalSumException {
        frodMonitor.isValidTransaction(transactionSum);
    }

    @Test(dataProvider = "testPositiveDataForFrodMonitorTest", dataProviderClass = DataProviderClass.class)
    public void isValidTransactionPositiveTest(double transactionSum) throws IllegalSumException {
        frodMonitor.isValidTransaction(transactionSum);
    }

}
