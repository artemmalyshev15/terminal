import Enums.Currency;
import Persons.Person;
import Terminal.Exceptions.IllegalSumException;
import Trarsactions.Transaction;
import Trarsactions.TransferAccountToAccount;
import Trarsactions.TransferCardToCard;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TransactionTest {

    private Person sender;
    private Person recipient;

    @BeforeMethod
    public void setUp() {
        sender = new Person("Иванов", "Иван", "Иванович",
                "12345678912345678912", "1234567890123456", 5000, 123);
        recipient = new Person("Петров", "Иван", "Петрович",
                "23456789123456789123", "2345678910234567", 10000, 111);
    }

    @Test(expectedExceptions = IllegalSumException.class)
    public void doTransferCardToCardTest() throws IllegalSumException {
        Transaction transaction = new TransferCardToCard(6000, Currency.RUR, sender, recipient);
        transaction.doTransfer();
    }

    @Test(expectedExceptions = IllegalSumException.class)
    public void doTransferAccToAccTest() throws IllegalSumException {
        Transaction transaction = new TransferAccountToAccount(6000, Currency.RUR, sender, recipient);
        transaction.doTransfer();
    }

    @Test
    public void updateSumCardToCardTest() throws IllegalSumException {
        Transaction transaction = new TransferCardToCard(500, Currency.RUR, sender, recipient);
        transaction.doTransfer();
        Assert.assertEquals(4500, sender.getBalance());
        Assert.assertEquals(10500, recipient.getBalance());
    }

    @Test
    public void updateSumAccToAccTest() throws IllegalSumException {
        Transaction transaction = new TransferAccountToAccount(500, Currency.RUR, sender, recipient);
        transaction.doTransfer();
        Assert.assertEquals(4500, sender.getBalance());
        Assert.assertEquals(10500, recipient.getBalance());
    }
}
