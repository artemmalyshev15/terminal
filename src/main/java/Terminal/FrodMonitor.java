package Terminal;

import Terminal.Exceptions.IllegalSumException;

public class FrodMonitor {

    private static final double MAX_TRANSACTION_SUM = 2500;

    public void isValidTransaction(double transactionSum) throws IllegalSumException {
        double remainderOfDivision = transactionSum % 100;

        if (remainderOfDivision != 0) {
            throw new IllegalSumException("Сумма перевода должна быть кратной 100.");
        }
        if (transactionSum > MAX_TRANSACTION_SUM) {
            throw new IllegalSumException("Максимальная сумма транзакции не должна превышать 2500.");
        }
    }
}
