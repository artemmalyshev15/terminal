package Terminal;

import Enums.Currency;
import Persons.Person;
import Terminal.Exceptions.AccountIsLockedException;
import Terminal.Exceptions.IllegalSumException;
import Terminal.Interfaces.Terminal;
import Trarsactions.Transaction;
import Trarsactions.TransferAccountToAccount;
import Trarsactions.TransferCardToCard;

public class TerminalImpl implements Terminal {

    private final TerminalServer terminalServer;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;

    private Person currentPerson;

    public TerminalImpl() {
        this.terminalServer = new TerminalServer();
        this.pinValidator = new PinValidator();
        this.frodMonitor = new FrodMonitor();
    }

    public void login(String accountNumber, int pinCode) throws AccountIsLockedException, IllegalAccessException {
        Person person = terminalServer.getPersonByNumberAccount(accountNumber);
        pinValidator.validate(person, pinCode);
        currentPerson = person;
    }

    public void doTransferAccToAcc(double transactionSum, Currency currency, String recipientAccountNumber) throws IllegalSumException, IllegalAccessException {
        try {
            terminalServer.isConnection();
            if (currentPerson == null) {
                throw new IllegalAccessException("Вы не авторизованы. Авторизуйтесь для работы в системе.");
            }
            Person person = terminalServer.getPersonByNumberAccount(recipientAccountNumber);
            frodMonitor.isValidTransaction(transactionSum);
            Transaction transaction = new TransferAccountToAccount(transactionSum, currency, currentPerson, person);
            transaction.doTransfer();
        } finally {
            currentPerson = null;
        }
    }

    public void doTransferCardToCard(double transactionSum, Currency currency, String recipientAccountNumber) throws IllegalAccessException, IllegalSumException {
        try {
            terminalServer.isConnection();
            if (currentPerson == null) {
                throw new IllegalAccessException("Вы не авторизованы. Авторизуйтесь для работы в системе.");
            }
            Person person = terminalServer.getPersonByNumberAccount(recipientAccountNumber);
            frodMonitor.isValidTransaction(transactionSum);
            Transaction transaction = new TransferCardToCard(transactionSum, currency, currentPerson, person);
            transaction.doTransfer();
        } finally {
            currentPerson = null;
        }
    }

    public TerminalServer getTerminalServer() {
        return terminalServer;
    }

    public PinValidator getPinValidator() {
        return pinValidator;
    }

    public FrodMonitor getFrodMonitor() {
        return frodMonitor;
    }
}
