package Terminal;

import Persons.Person;
import Terminal.Exceptions.AccountIsLockedException;

import java.time.Instant;
import java.util.Objects;

public class PinValidator {

    private Person currentPerson;
    private int numberOfAttempts;
    private Instant timeUnlock;

    public PinValidator() {

    }

    public void validate(Person person, int inputPin) throws AccountIsLockedException, IllegalAccessException {
        if (!Objects.equals(currentPerson, person)) {
            currentPerson = person;
            numberOfAttempts = 0;
            timeUnlock = null;
        }
        if (timeUnlock != null && timeUnlock.isAfter(Instant.now())) {
            throw new AccountIsLockedException("Ваш аккаунт будет разблокирован в " + timeUnlock);
        }
        timeUnlock = null;
        if (person.getPinCode() == inputPin) {
            numberOfAttempts = 0;
            return;
        }
        numberOfAttempts++;
        if (numberOfAttempts == 3) {
            timeUnlock = Instant.now().plusSeconds(5);
            throw new AccountIsLockedException("Ваш аккаунт заблокирован на 5 секунд.");
        }
        throw new IllegalAccessException("Ошибка доступа. Пароль не верный. Попробуйте еще раз.");
    }
}
