package Terminal;

import Persons.Person;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;

public class TerminalServer {

    private Map<String, Person> personMap = new HashMap<>();

    public TerminalServer() {
        personMap.put("12345678912345678912", new Person("Иванов", "Иван", "Иванович",
                "12345678912345678912", "1234567890123456", 5000, 123));
        personMap.put("23456789123456789123", new Person("Петров", "Иван", "Петрович",
                "23456789123456789123", "2345678910234567", 10000, 111));
    }

    public Person getPersonByNumberAccount(String numberAcc) {
        if (personMap.containsKey(numberAcc)) {
            return personMap.get(numberAcc);
        } else
            throw new NoSuchElementException("Пользователь с данным номером карты не существует.");
    }

    public void isConnection() throws IllegalAccessException {
        if (getRandomNumber() == 2) {
            throw new IllegalAccessException("Ошибка сети. Нет подключения к сети.");
        }
    }

    private int getRandomNumber() {
        int min = 0;
        int max = 2;
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public Map<String, Person> getPersonMap() {
        return personMap;
    }

    public void setPersonMap(Map<String, Person> personMap) {
        this.personMap = personMap;
    }
}
