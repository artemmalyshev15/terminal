package Terminal.Exceptions;

public class IllegalSumException extends Exception {
    public IllegalSumException(String message) {
        super(message);
    }
}
