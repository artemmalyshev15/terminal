package Terminal.Interfaces;

import Enums.Currency;
import Terminal.Exceptions.AccountIsLockedException;
import Terminal.Exceptions.IllegalSumException;

public interface Terminal {

    void login(String accountNumber, int pinCode) throws AccountIsLockedException, IllegalAccessException;

    void doTransferAccToAcc(double transactionSum, Currency currency, String recipientAccountNumber) throws IllegalSumException, IllegalAccessException;

    void doTransferCardToCard(double transactionSum, Currency currency, String recipientAccountNumber) throws IllegalAccessException, IllegalSumException;

}
