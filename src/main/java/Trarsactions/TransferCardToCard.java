package Trarsactions;

import Enums.Currency;
import Persons.Person;

public class TransferCardToCard extends Transaction {

    public TransferCardToCard(double transactionSum, Currency currency, Person sender, Person recipient) {
        super(transactionSum, currency, sender, recipient);
    }

    @Override
    public String toString() {
        return "ПЕРЕВОД С КАРТЫ НА КАРТУ: " +
                super.toString() +
                ", Номер карты отправителя: " + getSender().getCardNumber() +
                ", Номер карты получателя: " + getRecipient().getCardNumber();
    }
}
