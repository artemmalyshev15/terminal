package Trarsactions;

import Enums.Currency;
import Persons.Person;

public class TransferAccountToAccount extends Transaction {

    public TransferAccountToAccount(double transactionSum, Currency currency, Person sender, Person recipient) {
        super(transactionSum, currency, sender, recipient);
    }

    @Override
    public String toString() {
        return "ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " +
                super.toString() +
                ", Номер счета отправителя: " + getSender().getAccountNumber() +
                ", Номер счета получателя: " + getRecipient().getAccountNumber();
    }
}
