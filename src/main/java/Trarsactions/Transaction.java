package Trarsactions;

import Enums.Currency;
import Persons.Person;
import Terminal.Exceptions.IllegalSumException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public abstract class Transaction {

    private static int transactionNumber;
    private double transactionSum;
    private Currency currency;
    private Date transactionDate;

    private Person sender;
    private Person recipient;

    public Transaction(double transactionSum, Currency currency,
                       Person sender, Person recipient) {
        transactionNumber++;
        this.transactionSum = transactionSum;
        this.currency = currency;
        this.transactionDate = new Date();
        this.sender = sender;
        this.recipient = recipient;

    }

    public void doTransfer() throws IllegalSumException {
        if (getTransactionSum() > sender.getBalance()) {
            throw new IllegalSumException("Перевод совершить нельзя, недостаточно средств.");
        } else {
            updateSum(sender, recipient);
            System.out.println(this);
        }
    }

    private void updateSum(Person sender, Person recipient) {
        sender.setBalance(sender.getBalance() - transactionSum);
        recipient.setBalance(recipient.getBalance() + transactionSum);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (Double.compare(that.transactionSum, transactionSum) != 0) return false;
        if (currency != that.currency) return false;
        if (!Objects.equals(transactionDate, that.transactionDate))
            return false;
        if (!Objects.equals(sender, that.sender)) return false;
        return Objects.equals(recipient, that.recipient);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(transactionSum);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (transactionDate != null ? transactionDate.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        result = 31 * result + (recipient != null ? recipient.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Номер перевода: " + transactionNumber +
                ", Сумма: " + transactionSum +
                ", Валюта: " + currency +
                ", Дата: " + convertDate(transactionDate) +
                ", Отправитель: " + sender.getFIO() +
                ", Получатель: " + recipient.getFIO();
    }

    private String convertDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(date);
    }

    public static int getTransactionNumber() {
        return transactionNumber;
    }

    public static void setTransactionNumber(int transactionNumber) {
        Transaction.transactionNumber = transactionNumber;
    }

    public void setTransactionSum(double transactionSum) {
        this.transactionSum = transactionSum;
    }

    public double getTransactionSum() {
        return transactionSum;
    }

    public void setTransactionSum(int transactionSum) {
        this.transactionSum = transactionSum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Person getSender() {
        return sender;
    }

    public void setSender(Person sender) {
        this.sender = sender;
    }

    public Person getRecipient() {
        return recipient;
    }

    public void setRecipient(Person recipient) {
        this.recipient = recipient;
    }
}


