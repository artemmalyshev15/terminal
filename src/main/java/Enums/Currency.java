package Enums;

public enum Currency {
    RUR("РУБЛИ"),
    USD("ДОЛЛАР"),
    EUR("ЕВРО");

    private String currency;

    Currency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return currency;
    }
}
