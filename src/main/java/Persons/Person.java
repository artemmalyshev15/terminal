package Persons;

import java.util.Objects;

public class Person {

    private String surname;
    private String name;
    private String middleName;

    private String accountNumber;
    private String cardNumber;
    private double balance;

    private int pinCode;

    public Person(String surname, String name, String middleName, String accountNumber,
                  String cardNumber, double balance, int pinCode) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.accountNumber = accountNumber;
        this.cardNumber = cardNumber;
        this.balance = balance;
        this.pinCode = pinCode;
    }

    public String getFIOtoUpperCase() {
        return getFIO().toUpperCase();
    }

    public String getFIOtoLowerCase() {
        return getFIO().toLowerCase();
    }

    public String getFIO() {
        return surname + " " + name + " " + middleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (Double.compare(person.balance, balance) != 0) return false;
        if (pinCode != person.pinCode) return false;
        if (!Objects.equals(surname, person.surname)) return false;
        if (!Objects.equals(name, person.name)) return false;
        if (!Objects.equals(middleName, person.middleName)) return false;
        if (!Objects.equals(accountNumber, person.accountNumber))
            return false;
        return Objects.equals(cardNumber, person.cardNumber);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = surname != null ? surname.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (accountNumber != null ? accountNumber.hashCode() : 0);
        result = 31 * result + (cardNumber != null ? cardNumber.hashCode() : 0);
        temp = Double.doubleToLongBits(balance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + pinCode;
        return result;
    }

    @Override
    public String toString() {
        return "Фамилия: " + surname +
                ", Имя: " + name +
                ", Отчество: " + middleName +
                ", Номер счета: " + accountNumber +
                ", Номер карты: " + cardNumber +
                ", Количество средств: " + balance +
                ", Пин-код: " + pinCode;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }
}
