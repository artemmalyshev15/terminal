import Enums.Currency;
import Terminal.Exceptions.AccountIsLockedException;
import Terminal.Exceptions.IllegalSumException;
import Terminal.TerminalImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        TerminalImpl terminal = new TerminalImpl();
        System.out.println(terminal.getTerminalServer().getPersonByNumberAccount("12345678912345678912"));
        System.out.println(terminal.getTerminalServer().getPersonByNumberAccount("23456789123456789123"));
        while (true) {
            try {
                System.out.println("Введите, пожалуйста, пароль: ");
                terminal.login("12345678912345678912", Integer.parseInt(scanner.nextLine())); //Правильный 123
                terminal.doTransferAccToAcc(500, Currency.RUR, "23456789123456789123");
                System.out.println(terminal.getTerminalServer().getPersonByNumberAccount("23456789123456789123"));
                return;
            } catch (AccountIsLockedException | IllegalAccessException | IllegalSumException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
